<?php
/**
	Plugin name: C7 Toastmasters
 */

class C7_Toastmasters {


	public function __construct() {
		$this->hooks();
	}

	public function hooks() {
		add_shortcode( 'clubs', array( $this, 'show_clubs' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_filter( 'the_content', array( $this, 'add_club_info' ) );
	}

	public function add_club_info( $content ) {
		$parent_id   = wp_get_post_parent_id( get_the_ID() );
		$parent_post = get_post( $parent_id );
		$info = '';
		if ( $parent_post->post_name === 'clubs' ) {
			ob_start();
			?>
			<div class="club-informatie">
				<table>
					<tbody>
						<tr>
							<td>Language</td>
							<td><?php the_field( 'voertaal' ); ?></td>
						</tr>
						<tr>
							<td>Meeting&nbsp;Days</td>
							<td><?php the_field( 'clubdagen' ); ?></td>
						</tr>
						<tr>
							<td>Times</td>
							<td><?php the_field( 'tijden' ); ?></td>
						</tr>
						<tr>
							<td>Location</td>
							<td><?php the_field( 'locatie' ); ?></td>
						</tr>
						<tr>
							<td>TM&nbsp;Site</td>
							<td>
									<?php if ( get_field( 'clubwebsite' ) ) : ?>
								
								<a href="<?php the_field( 'clubwebsite' ); ?>" target="_blank" rel="noopener"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/easyspeak.png" alt="EasySpeak" title="EasySpeak" width="61" height="20" /></a>
								
									<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>Social&nbsp;Media</td>
							<td>
									<?php if ( get_field( 'facebook' ) ) : ?>
								
								<a href="<?php the_field( 'facebook' ); ?>" title="Facebook" target="_blank" rel="noopener"><i class="fa fa-facebook-official fa-lg" aria-hidden="true"></i></a>
								
									<?php endif; ?>
									<?php if ( get_field( 'meetup' ) ) : ?>
								
								<a href="<?php the_field( 'meetup' ); ?>" title="Meetup"  target="_blank" rel="noopener"><i class="fa fa-meetup fa-lg" aria-hidden="true"></i></a>
								
									<?php endif; ?>
									<?php if ( get_field( 'linkedin' ) ) : ?>
								
								<a href="<?php the_field( 'linkedin' ); ?>" title="LinkedIn"  target="_blank" rel="noopener"><i class="fa fa-linkedin-square  fa-lg" aria-hidden="true"></i></a>
								
									<?php endif; ?>
									<?php if ( get_field( 'twitter' ) ) : ?>
								
								<a href="<?php the_field( 'twitter' ); ?>" title="Twitter"  target="_blank" rel="noopener"><i class="fa fa fa-twitter  fa-lg" aria-hidden="true"></i></a>
								
									<?php endif; ?>
									<?php if ( get_field( 'youtube' ) ) : ?>
								
								<a href="<?php the_field( 'youtube' ); ?>" title="YouTube"  target="_blank" rel="noopener"><i class="fa fa-youtube fa-lg" aria-hidden="true"></i></a>
								
									<?php endif; ?>
									<?php if ( get_field( 'instagram' ) ) : ?>
								
								<a href="<?php the_field( 'instagram' ); ?>" title="Instagram"  target="_blank" rel="noopener"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
								
									<?php endif; ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php
			$info = ob_get_clean();

		}
		return $info . $content;
	}

	public function enqueue_scripts() {
		wp_enqueue_style( 'c7-toastmasters', plugin_dir_url( __FILE__ ) . '/style.css', array(), filemtime( plugin_dir_path( __FILE__ ) . '/style.css' ) );
	}

	public function show_clubs() {
		$posts = get_posts(
			array(
				'post_type'   => 'page',
				'parent'      => 2861,
				'numberposts' => -1,
				'lang'        => 'nl',
			)
		);

		$output = '';

		$output .= '<div class="clubs">';

		foreach ( $posts as $post ) {
			if ( $post->post_parent === 2861 ) {
				$image = '';
				if ( has_post_thumbnail( $post->ID ) ) {
					$image = get_the_post_thumbnail_url( $post->ID );
				} else {
					$image = plugin_dir_url( __FILE__ ) . '/club-placeholder.png';
				}
				$output .= '<a class="item" style="background-image: url(' . $image . ');" href="' . get_permalink( $post ) . '"><span class="title">' . $post->post_title . '</span></a>';
			}
		}

		$output .= '</div>';

		return $output;
	}
}

new C7_Toastmasters();
